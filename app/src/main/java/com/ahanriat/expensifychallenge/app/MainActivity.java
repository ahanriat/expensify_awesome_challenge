package com.ahanriat.expensifychallenge.app;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.ahanriat.expensifychallenge.app.managers.ActivityManager;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;


/**
 * The main activity, hosting two fragments to display transaction list and add transaction.
 *
 * NOTE: A cool things to do: add a third Fragment to display the current selected transaction
 */
public class MainActivity extends ActionBarActivity {

    private CharSequence   mTitle;
    private SessionManager mSessionManager;

    // Maybe I should replace this boolean with an enum
    // to be more clear when having more than 2 Fragments
    private Boolean                 mOnTransactionList       = true;
    private TransactionListFragment mTransactionListFragment = null;
    private ActivityManager mActivityManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(this);
        mActivityManager = new ActivityManager(this);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);

        checkLogin();

        setContentView(R.layout.activity_main);
        displayHomeFragment();
    }


    public void displayHomeFragment() {
        mOnTransactionList = true;
        mTitle = getResources().getString(R.string.title_fragment_transactions_list);
        invalidateOptionsMenu();
        mTransactionListFragment = new TransactionListFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top)
                .replace(R.id.container, mTransactionListFragment)
                .commit();
    }

    public void onAddTransaction() {
        mOnTransactionList = false;
        mTitle = getResources().getString(R.string.add_transaction_title);
        invalidateOptionsMenu();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom)
                .replace(R.id.container, new AddTransactionFragment())
                .commit();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        actionBar.setDisplayHomeAsUpEnabled(!mOnTransactionList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int menuId = (mOnTransactionList) ? R.menu.transaction_list : R.menu.add_transaction;
        getMenuInflater().inflate(menuId, menu);
        restoreActionBar();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                displayHomeFragment();
                return true;
            case R.id.action_add:
                onAddTransaction();
                return true;
            case R.id.action_refresh:
                onRefresh();
                return true;
            case R.id.action_logout:
                onLogout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRefresh() {
        mTransactionListFragment.onRefresh();
    }

    public void onLogout() {
        mActivityManager.goLogin(false);
    }

    /**
     * Redirect to LoginActivity if the user doesn't have credentials
     */
    public void checkLogin() {
        if (!mSessionManager.checkSession()) {
            mActivityManager.goLogin(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (mOnTransactionList)
            super.onBackPressed();
        else
            displayHomeFragment();
    }
}
