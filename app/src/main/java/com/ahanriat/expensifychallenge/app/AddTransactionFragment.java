package com.ahanriat.expensifychallenge.app;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ahanriat.expensifychallenge.app.Exceptions.CredentialException;
import com.ahanriat.expensifychallenge.app.helpers.Utils;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;
import com.ahanriat.expensifychallenge.app.models.Transaction;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

/**
 * The fragment allowing to create and add a new Transaction
 */
public class AddTransactionFragment extends Fragment {

    private SessionManager                     mSessionManager;
    private TextView                           mDate;
    private EditText                           mAmount;
    private EditText                           mComment;
    private EditText                           mMerchant;
    private Button                             mButtonValid;
    private DatePickerDialog.OnDateSetListener mOnDateSetListenner;
    private Calendar                           mCalendar;
    private CreateTransaction                  asyncTask;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mContentView = inflater.inflate(R.layout.fragment_add_transaction, container, false);
        mAmount = (EditText) mContentView.findViewById(R.id.prompt_amount);
        mMerchant = (EditText) mContentView.findViewById(R.id.prompt_merchant);
        mButtonValid = (Button) mContentView.findViewById(R.id.action_add);
        mComment = (EditText) mContentView.findViewById(R.id.prompt_comment);
        mDate = (TextView) mContentView.findViewById(R.id.transaction_date);
        mSessionManager = new SessionManager(getActivity());


        initListeners();

        mCalendar = Calendar.getInstance();
        mDate.setText(Utils.dateToString(new Date(), "yyyy-MM-dd"));

        return mContentView;
    }

    public void initListeners() {
        mButtonValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkFields()) {
                    asyncTask = new CreateTransaction();
                    asyncTask.execute();
                }
            }
        });

        mOnDateSetListenner = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                mDate.setText(Utils.dateToString(mCalendar.getTime(), "yyyy-MM-dd"));
                mCalendar.set(year, month, day);
            }
        };

        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date date = new Date();
                mCalendar.setTime(date);

                DatePickerDialog datePicker = new DatePickerDialog(getActivity(), mOnDateSetListenner
                        , mCalendar.get(Calendar.YEAR)
                        , mCalendar.get(Calendar.MONTH)
                        , mCalendar.get(Calendar.DAY_OF_MONTH));
                datePicker.show();
            }
        });
    }


    protected boolean checkFields() {
        if (mMerchant.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_missing_merchant),
                    Toast.LENGTH_LONG).show();
            return false;
        }
        if (mAmount.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_missing_amount),
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    protected void clearForm() {
        mMerchant.getEditableText().clear();
        mAmount.getEditableText().clear();
        mComment.getEditableText().clear();
    }

    protected class CreateTransaction extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected String doInBackground(String... params) {
            float amount = Float.valueOf(mAmount.getText().toString());
            String merchant = mMerchant.getText().toString();
            Date created = mCalendar.getTime();
            Transaction transaction = new Transaction();
            transaction.setAmount(amount);
            transaction.setMerchantName(merchant);
            transaction.setCreated(created);
            transaction.setCurrency("USD");
            transaction.setComments(mComment.getText().toString());

            try {
                transaction.save(mSessionManager);
                return getResources().getString(R.string.success_transaction_save);
            } catch (CredentialException e) {
                return getResources().getString(R.string.error_transaction_save);
            } catch (UnsupportedEncodingException e) {
                return getResources().getString(R.string.error_mysterious);
            }
        }
        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            getActivity().setProgressBarIndeterminateVisibility(false);
            clearForm();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setProgressBarIndeterminateVisibility(false);
        if (asyncTask != null) {
            asyncTask.cancel(false);
        }
    }

}
