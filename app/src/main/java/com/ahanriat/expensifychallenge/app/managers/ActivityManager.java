package com.ahanriat.expensifychallenge.app.managers;


import android.content.Context;
import android.content.Intent;

import com.ahanriat.expensifychallenge.app.LoginActivity;
import com.ahanriat.expensifychallenge.app.MainActivity;
import com.ahanriat.expensifychallenge.app.helpers.Constants;

/**
 * Factorise the code to switch between activities
 */
public class ActivityManager {

    private Context mContext;

    public ActivityManager(Context context) {
        mContext = context;
    }

    /**
     * Launch the login activity, clear the activity stack
     * @param keepMail if true, put an extra string in the intent, corresponding to the email
     *                 of the user. It is useful when the token expired.
     */
    public void goLogin(boolean keepMail) {
        SessionManager sessionManager = new SessionManager(mContext);
        Intent loginIntent = new Intent(mContext, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Used to fill email field, nice when Token session has expired
        if (keepMail) {
            loginIntent.putExtra(Constants.EMAIL, sessionManager.getSessionEmail());
        }

        sessionManager.clearSession();
        mContext.startActivity(loginIntent);
    }

    /**
     * Launch the main activity, clear the activity stack
     */
    public void goMainActivity() {
        Intent mainActivityIntent = new Intent(mContext, MainActivity.class);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(mainActivityIntent);
    }
}
