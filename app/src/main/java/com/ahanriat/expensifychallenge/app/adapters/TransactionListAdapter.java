package com.ahanriat.expensifychallenge.app.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ahanriat.expensifychallenge.app.R;
import com.ahanriat.expensifychallenge.app.collections.TransactionList;
import com.ahanriat.expensifychallenge.app.helpers.Utils;
import com.ahanriat.expensifychallenge.app.models.Transaction;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class TransactionListAdapter extends BaseAdapter {

    protected Context                mContext;
    protected ArrayList<Transaction> mTransactionList;
    protected LayoutInflater         mInflater;

    public TransactionListAdapter(Context context, ArrayList<Transaction> transactionList) {
        mContext = context;
        mTransactionList = transactionList;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mTransactionList.size();
    }

    @Override
    public Object getItem(int i) {
        return mTransactionList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mTransactionList.get(i).getTransactionId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;

        // Not recycled
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_transaction, null);

            holder = new ViewHolder();
            holder.mMerchant = (TextView) convertView.findViewById(R.id.transaction_merchant_name);
            holder.mComment = (TextView) convertView.findViewById(R.id.transaction_comment);
            holder.mDate = (TextView) convertView.findViewById(R.id.transaction_date);
            holder.mAmount = (TextView) convertView.findViewById(R.id.transaction_amount);

            convertView.setTag(holder);
        } else {
            // Just get the viewHolder
            holder = (ViewHolder) convertView.getTag();
        }

        Transaction transaction = (Transaction) getItem(i);

        float amount = transaction.getAmount();
        holder.mMerchant.setText(transaction.getMerchantName());
        holder.mAmount.setText(String.valueOf(amount));
        holder.mDate.setText(Utils.dateToString(transaction.getCreated(), "yyyy-MM-dd"));
        holder.mComment.setText(transaction.getComment());

        // Set the color of the amount depending on the value, not very sure about colors ;)
        if (amount > 0) {
            holder.mAmount.setTextColor(0xffffbbbb);
        }
        else {
            holder.mAmount.setTextColor(0xffccffcc);
        }

        return convertView;
    }

    static class ViewHolder {
        public TextView mMerchant;
        public TextView mDate;
        public TextView mComment;
        public TextView mAmount;
    }
}
