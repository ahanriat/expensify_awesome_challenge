package com.ahanriat.expensifychallenge.app.managers;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.ahanriat.expensifychallenge.app.helpers.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Store and manage user information through the application, such as email and authenticate token
 */
public class SessionManager {

    private Context           mContext;
    private Editor            mEditor;
    private SharedPreferences mSharedPreferences;

    private static String PREFERENCES_KEY = "awesome.preferences";
    private static String ACCOUNT_KEY     = "awesome.account";
    private static String EMAIL_KEY       = "awesome.email";
    private static String TOKEN_KEY       = "awesome.token";

    public SessionManager(Context context) {
        this.mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void createSession(JSONObject json) {
        String accountId = null, email = null, authenticationToken = null;
        try {
            accountId = json.getString(Constants.ACCOUNT_ID);
            email = json.getString(Constants.EMAIL);
            authenticationToken = json.getString(Constants.AUTH_TOKEN);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        createSession(accountId, email, authenticationToken);
    }

    public void createSession(String accountId, String email, String authenticationToken) {
        mEditor.putString(ACCOUNT_KEY, accountId);
        mEditor.putString(EMAIL_KEY, email);
        mEditor.putString(TOKEN_KEY, authenticationToken);
        mEditor.commit();
    }

    public void clearSession() {
        mEditor.clear();
        mEditor.commit();
    }

    /**
     * @return true if user info look correct
     * NOTE: a nice feature could be to add a request to the server to check
     *       if the token is still valid
     */
    public boolean checkSession() {
        return !(getAccountId() == null
                || getSessionEmail() == null
                || getSessionToken() == null);
    }

    public String getAccountId() {
        return mSharedPreferences.getString(ACCOUNT_KEY, null);
    }

    public String getSessionToken() {
        return mSharedPreferences.getString(TOKEN_KEY, null);
    }

    public String getSessionEmail() {
        return mSharedPreferences.getString(EMAIL_KEY, null);
    }


}
