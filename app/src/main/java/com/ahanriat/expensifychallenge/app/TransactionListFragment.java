package com.ahanriat.expensifychallenge.app;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ahanriat.expensifychallenge.app.Exceptions.CredentialException;
import com.ahanriat.expensifychallenge.app.adapters.TransactionListAdapter;
import com.ahanriat.expensifychallenge.app.collections.TransactionList;
import com.ahanriat.expensifychallenge.app.helpers.Constants;
import com.ahanriat.expensifychallenge.app.helpers.Utils;
import com.ahanriat.expensifychallenge.app.managers.ActivityManager;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;

import java.util.Date;
import java.util.HashMap;

/**
 * There is currently no pagination, and this could be a great asset as there are thousands of
 * transactions. I could use startDate and endDate to perform this.
 */
public class TransactionListFragment extends Fragment {

    private static final String TAG = "TransactionList";

    private ListView       mTransactionListView;
    private RelativeLayout mTransactionPlaceholder;

    private SessionManager  mSessionManager;
    private TransactionList mTransactionList;

    private GetTransactionList asyncTask;

    private static final String BUNDLE_KEY_TRANSACTIONS = "transactionslist";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mContentView = inflater.inflate(R.layout.fragment_transaction_list, container, false);
        mTransactionListView = (ListView) mContentView.findViewById(R.id.transaction_list_view);
        mTransactionPlaceholder = (RelativeLayout) mContentView.findViewById(R.id.transaction_placeholder);

        if (savedInstanceState != null) {
            mTransactionList = savedInstanceState.getParcelable(BUNDLE_KEY_TRANSACTIONS);
        } else {
            mTransactionList = new TransactionList();
            refreshTransactionList();
        }

        return mContentView;
    }

    public void refreshTransactionList() {
        asyncTask = new GetTransactionList();
        asyncTask.execute();
    }

    public void onRefresh() {
        refreshTransactionList();
    }


    private void renderTransactionListView() {
        // If there is no transaction, render a placeholder
        if (mTransactionList.getTransactions().isEmpty()) {
            mTransactionPlaceholder.setVisibility(View.VISIBLE);
            return;
        } else {
            mTransactionPlaceholder.setVisibility(View.GONE);
        }

        TransactionListAdapter adapter = new TransactionListAdapter(getActivity(), mTransactionList.getTransactions());
        mTransactionListView.setAdapter(adapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setProgressBarIndeterminateVisibility(false);
        if (asyncTask != null) {
            asyncTask.cancel(false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_KEY_TRANSACTIONS, mTransactionList);
    }

    private class GetTransactionList extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> options = new HashMap<String, String>();

            // As I've not implemented pagination, let's get the last year transactions
            Date now = new Date();
            Date aYearAgo = Utils.aYearAgo();
            options.put(Constants.END_DATE, Utils.dateToString(now, "yyyy-MM-dd"));
            options.put(Constants.START_DATE, Utils.dateToString(aYearAgo, "yyyy-MM-dd"));
            try {
                mTransactionList.fetch(mSessionManager, options);
            } catch (CredentialException e) {
                // Go to Login
                ActivityManager activityManager = new ActivityManager(getActivity());
                activityManager.goLogin(true);
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            renderTransactionListView();
            getActivity().setProgressBarIndeterminateVisibility(false);
        }
    }

    ;

}
