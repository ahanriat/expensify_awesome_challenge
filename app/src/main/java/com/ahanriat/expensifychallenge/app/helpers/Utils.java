package com.ahanriat.expensifychallenge.app.helpers;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Provides some basic tools
 */
public class Utils {

    /**
     * Convert an InputStream into a string
     */
    public static String convertStream(InputStream inputStream) {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    /**
     * Convert a string into a date
     *
     * @param sDate  the String representing the date
     * @param format the format of the string ex: "yyyy-MM-yy"
     */
    public static Date stringToDate(String sDate, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(sDate);
    }

    /**
     * Convert a Date to a string
     *
     * @param date
     * @param format the format of the string ex: "yyyy-MM-yy"
     */
    public static String dateToString(Date date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }

    /**
     *
     * @return the date a year ago
     */
    public static Date aYearAgo() {
        Long now = new Date().getTime();
        Long aYear = (long) 3600 * 1000 * 24 * 365;
        return new Date(now - aYear);
    }
}
