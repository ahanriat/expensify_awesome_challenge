package com.ahanriat.expensifychallenge.app.helpers;


import android.util.Log;

import com.ahanriat.expensifychallenge.app.Exceptions.CredentialException;
import com.ahanriat.expensifychallenge.app.Exceptions.EmailInvalidException;
import com.ahanriat.expensifychallenge.app.Exceptions.PasswordInvalidException;
import com.ahanriat.expensifychallenge.app.Exceptions.TokenInvalidException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RestHelper {

    private final static String TAG = "RestHelper";

    /**
     *   Try to authenticate with the server.
     *   @return a JSONObject containing the server response
     *
     *   Can throw a CredentialException, if given credentials are incorrect
     */
    public static JSONObject getAuthentificationToken(String email, String password) throws JSONException, CredentialException {
        JSONObject result = new JSONObject();
        try {
            URL url = new URL(Constants.API_URL
                    + "?" + Constants.COMMAND + "=" + Constants.COMMAND_AUTHENTICATE
                    + "&" + Constants.PARTNER_NAME + "=" + Constants.PARTNER_NAME_VALUE
                    + "&" + Constants.PARTNER_PASSWORD + "=" + Constants.PARTNER_PASSWORD_VALUE
                    + "&" + Constants.PARTNER_USER_ID + "=" + email
                    + "&" + Constants.PARTNER_USER_SECRET + "=" + password);

            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(connection.getInputStream());
            result = new JSONObject(Utils.convertStream(in));

            // jsonCode

            int httpStatus = result.getInt("jsonCode");
            checkStatus(httpStatus);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *   Performs an http GET request with the given URL
     *   @return a JSONObject containing the server response
     *
     *   Can throw a CredentialException if the token has expired or is invalid
     */
    public static JSONObject doGet(String addr) throws JSONException, CredentialException {
        JSONObject result = new JSONObject();
        try {
            URL url = new URL(addr);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(connection.getInputStream());
            result = new JSONObject(Utils.convertStream(in));

            int httpStatus = result.getInt("jsonCode");
            checkStatus(httpStatus);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }

    /**
     *   Performs an http POST request with the given addr and parameters
     *   @return a JSONObject containing the server response
     *
     *   Can throw a CredentialException if the token has expired or is invalid
     */
    public static JSONObject doPost(String addr, String urlParameters) throws JSONException, CredentialException {
        JSONObject result = new JSONObject();
        try {
            URL url = new URL(addr);

            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");

            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            InputStream in = new BufferedInputStream(connection.getInputStream());
            result = new JSONObject(Utils.convertStream(in));

            int httpStatus = result.getInt("jsonCode");
            checkStatus(httpStatus);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }


    /**
     *   Check the status of the request, throwing the
     *   corresponding Exception if needed
     */
    protected static void checkStatus(int status) throws CredentialException {
        Log.d(TAG, String.valueOf(status));
        switch (status) {
            case Constants.EMAIL_NOT_VALIDATE:
                throw new EmailInvalidException();
            case Constants.WRONG_PASSWORD:
                throw new PasswordInvalidException();
            case Constants.TOKEN_EXPIRED:
            case Constants.MALFORMED_TOKEN:
                throw new TokenInvalidException();
        }
    }
}