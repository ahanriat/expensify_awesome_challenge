package com.ahanriat.expensifychallenge.app;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.ahanriat.expensifychallenge.app.managers.ActivityManager;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;

/**
 * Manage the authenticate of the user
 *
 * NOTE: This activity only use fragments, in case of we add a sign up fragment,
 *       or an onBoarding Fragment.
 *       (I'm aware only a classic activity could be enough ;)
 */
public class LoginActivity extends FragmentActivity {

    private SessionManager mSessionManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
        setContentView(R.layout.activity_login);
        ActionBar actionBar = getActionBar();
        actionBar.hide();

        mSessionManager = new SessionManager(this);
        if (mSessionManager.checkSession()) {
            ActivityManager activityManager = new ActivityManager(this);
            activityManager.goMainActivity();
        } else {
            setLoginFragment();
        }

    }

    private void setLoginFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.login_activity_content, new LoginFragment())
                .commit();
    }


}
