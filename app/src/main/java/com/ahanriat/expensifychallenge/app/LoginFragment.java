package com.ahanriat.expensifychallenge.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ahanriat.expensifychallenge.app.Exceptions.CredentialException;
import com.ahanriat.expensifychallenge.app.Exceptions.EmailInvalidException;
import com.ahanriat.expensifychallenge.app.Exceptions.PasswordInvalidException;
import com.ahanriat.expensifychallenge.app.helpers.Constants;
import com.ahanriat.expensifychallenge.app.helpers.RestHelper;
import com.ahanriat.expensifychallenge.app.managers.ActivityManager;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";
    private ProgressBar mProgress;
    private Button      mSignin;
    private EditText    mLogin;
    private EditText    mPassword;
    private View        mContentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.fragment_login, container, false);
        mSignin = (Button) mContentView.findViewById(R.id.action_sign_in);
        mLogin = (EditText) mContentView.findViewById(R.id.prompt_email);
        mPassword = (EditText) mContentView.findViewById(R.id.prompt_password);
        mProgress = (ProgressBar) mContentView.findViewById(R.id.progress);

        initListeners();

        // Fill the email input
        Intent intent = getActivity().getIntent();
        String email = intent.getStringExtra(Constants.EMAIL);
        if (email != null) {
            mLogin.setText(email);
            mPassword.requestFocus();
            Toast.makeText(getActivity(), getResources().getString(R.string.session_expired), Toast.LENGTH_LONG).show();
        }


        return mContentView;
    }

    private void login() {
        String login = mLogin.getText().toString();
        String password = mPassword.getText().toString();

        if (login.isEmpty() || password.isEmpty()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_input_form),
                    Toast.LENGTH_LONG).show();
            return;
        }

        CheckCredentials checkCredentials = new CheckCredentials();

        checkCredentials.execute(login, password);
        // Use this for dev ;)
        //checkCredentials.execute("expensifytest@mailinator.com", "hire_me");
    }

    private void initListeners() {
        mSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    login();
                    handled = true;
                }
                return handled;
            }
        });
    }

    private class CheckCredentials extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setVisibility(View.VISIBLE);
        }


        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject json = RestHelper.getAuthentificationToken(params[0], params[1]);
                if (json.getInt("httpCode") == Constants.OK) {
                    SessionManager sessionManager = new SessionManager(getActivity());
                    sessionManager.createSession(json);

                    ActivityManager activityManager = new ActivityManager(getActivity());
                    activityManager.goMainActivity();
                } else {
                    return getResources().getString(R.string.error_credentials);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return getResources().getString(R.string.error_mysterious);
            } catch (EmailInvalidException e) {
                return getResources().getString(R.string.error_invalid_email);
            } catch (PasswordInvalidException e) {
                return getResources().getString(R.string.error_invalid_password);
            } catch (CredentialException e) {
                return getResources().getString(R.string.error_credentials);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            }
            mProgress.setVisibility(View.INVISIBLE);
        }
    };
}
