package com.ahanriat.expensifychallenge.app.Exceptions;


public class CredentialException extends Exception {

    public CredentialException() {
        super();
    }

    public CredentialException(String msg) {
        super(msg);
    }


}
