package com.ahanriat.expensifychallenge.app.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.ahanriat.expensifychallenge.app.Exceptions.CredentialException;
import com.ahanriat.expensifychallenge.app.helpers.Constants;
import com.ahanriat.expensifychallenge.app.helpers.RestHelper;
import com.ahanriat.expensifychallenge.app.helpers.Utils;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Date;

import static com.ahanriat.expensifychallenge.app.helpers.Utils.stringToDate;

public class Transaction implements Parcelable{

    protected String mMerchantName;
    protected int    mAmount;
    protected Date   mCreated;
    protected String mCurrency = "USD";
    protected String mComments;
    protected long mTransactionId;

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getMerchantName());
        parcel.writeInt(mAmount);
        parcel.writeString(Utils.dateToString(mCreated, "yyyy-MM-dd"));
        parcel.writeString(mCurrency);
        parcel.writeString(mComments);
        parcel.writeLong(mTransactionId);
    }

    public Transaction(Parcel in) {
        mMerchantName = in.readString();
        mAmount = in.readInt();
        try {
            mCreated = Utils.stringToDate(in.readString(), "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mCurrency = in.readString();
        mComments = in.readString();
        mTransactionId = in.readLong();
    }

    public Transaction(JSONObject json) throws JSONException, ParseException {
        buildTransaction(json);
    }


    public Transaction() {
        this.mCreated = new Date();
    }

    public int save(SessionManager sessionManager) throws CredentialException, UnsupportedEncodingException {
        String url = Constants.API_URL
                + "?" + Constants.COMMAND + "=" + Constants.CREATE_TRANSACTION
                + "&" + Constants.AUTH_TOKEN + "=" + sessionManager.getSessionToken()
                + "&" + Constants.CREATED + "=" + Utils.dateToString(mCreated, "yyyy-MM-dd")
                + "&" + Constants.AMOUNT + "=" + String.valueOf(mAmount)
                + "&" + Constants.MERCHANT + "=" + URLEncoder.encode(mMerchantName, "UTF-8")
                + "&" + Constants.COMMENT + "=" + URLEncoder.encode(mComments, "UTF-8")
                + "&" + Constants.CURRENCY + "=" + mCurrency;

        try {
            buildTransaction(RestHelper.doGet(url));
        } catch (JSONException e) {
            e.printStackTrace();
            return 1;
        } catch (ParseException e) {
            e.printStackTrace();
            return 1;
        }


        return 0;
    }

    protected void buildTransaction(JSONObject json) throws JSONException, ParseException {
        this.mMerchantName = json.getString(Constants.MERCHANT);
        this.mAmount = json.getInt(Constants.AMOUNT);
        this.mCreated = stringToDate(json.getString(Constants.CREATED), "yyyy-mm-dd");
        this.mTransactionId = json.getLong(Constants.TRANSACTION_ID);
        this.mComments = json.getString(Constants.COMMENT);
    }


    @Override
    public int describeContents() {
        return 0;
    }




    //  Setters & getters

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }

    public String getMerchantName() {
        return mMerchantName;
    }

    public void setMerchantName(String mMerchantName) {
        this.mMerchantName = mMerchantName;
    }

    public float getAmount() {
        return (float) mAmount / 100;
    }

    public void setAmount(float mAmount) {
        this.mAmount = (int) (mAmount * 100);
    }

    public Date getCreated() {
        return mCreated;
    }

    public void setCreated(Date mCreated) {
        this.mCreated = mCreated;
    }

    public String getComment() {
        return mComments;
    }

    public void setComments(String mComments) {
        this.mComments = mComments;
    }

    public long getTransactionId() {
        return mTransactionId;
    }

    public void setTransactionId(long mTransactionId) {
        this.mTransactionId = mTransactionId;
    }


}
