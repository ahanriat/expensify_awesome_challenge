package com.ahanriat.expensifychallenge.app.helpers;


/**
 *   Here are all constants used in the app
 */
public class Constants {

    // JSON code values
    public final static int OK                        = 200;
    public final static int ALREADY_EXISTS            = 300;
    public final static int UNRECOGNIZED_COMMAND      = 400;
    public final static int WRONG_PASSWORD            = 401;
    public final static int MISSING_COMMAND           = 402;
    public final static int ACCOUNT_NOT_FOUND         = 404;
    public final static int EMAIL_NOT_VALIDATE        = 405;
    public final static int MALFORMED_TOKEN           = 407;
    public final static int TOKEN_EXPIRED             = 408;
    public final static int INSUFFICIENT_PRIVILEGES   = 411;
    public final static int ABORTED                   = 500;
    public final static int DB_TRANSACTION_ERROR      = 501;
    public final static int QUERY_ERROR               = 502;
    public final static int QUERY_RESPONSE_ERROR      = 503;
    public final static int UNRECOGNIZED_OBJECT_STATE = 504;

    // APP keys
    public final static String API_URL          = "https://api.expensify.com";
    public final static String PARTNER_NAME     = "partnerName";
    public final static String PARTNER_NAME_VALUE = "applicant";
    public final static String PARTNER_PASSWORD = "partnerPassword";
    public final static String PARTNER_PASSWORD_VALUE = "d7c3119c6cdab02d68d9";
    public final static String PARTNER_USER_ID = "partnerUserID";
    public final static String PARTNER_USER_SECRET = "partnerUserSecret";

    // JSON hash
    public final static String TRANSACTION_ID = "transactionID";
    public final static String EMAIL          = "email";
    public final static String AUTH_TOKEN     = "authToken";
    public final static String ACCOUNT_ID     = "accountID";

    public final static String COMMAND        = "command";
    public final static String COMMAND_GET    = "Get";
    public final static String COMMAND_AUTHENTICATE = "Authenticate";

    public final static String START_DATE     = "startDate";
    public final static String END_DATE       = "endDate";
    public final static String AMOUNT         = "amount";
    public final static String COMMENT        = "comment";
    public final static String CREATED        = "created";
    public final static String CURRENCY       = "currency";
    public final static String MERCHANT       = "merchant";
    public final static String PAGE_NAME      = "pageName";
    public final static String RECEIPTS       = "receipts";
    public final static String TRANSACTION_LIST   = "transactionList";
    public final static String RETURN_VALUE_LIST  = "returnValueList";
    public final static String CREATE_TRANSACTION = "CreateTransaction";

}
