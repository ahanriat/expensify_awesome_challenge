package com.ahanriat.expensifychallenge.app.collections;


import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.ahanriat.expensifychallenge.app.Exceptions.CredentialException;
import com.ahanriat.expensifychallenge.app.helpers.Constants;
import com.ahanriat.expensifychallenge.app.helpers.RestHelper;
import com.ahanriat.expensifychallenge.app.managers.SessionManager;
import com.ahanriat.expensifychallenge.app.models.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

public class TransactionList extends  ArrayList<Parcelable> implements Parcelable{

    private final static String TAG = "TransactionList";

    protected ArrayList<Transaction> mTransactions = new ArrayList<Transaction>();

    private final static String KEY = "TRANSACTIONS_LIST_KEY";

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(this.size());
        for (int i = 0; i < this.size() ; i++) {
            parcel.writeParcelable(this.get(i),0);
        }
    }


    public TransactionList() {
        super();
    }

    public TransactionList(Parcel in) {
        super();
        this.clear();
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            this.add(in.readParcelable(Transaction.class.getClassLoader()));
        }
    }

    public TransactionList(JSONObject transactions) throws JSONException {
        super();
        buildTransactions(transactions);
    }

    public void fetch(SessionManager sessionManager, HashMap<String, String> options) throws CredentialException {

        String url = Constants.API_URL;
        url += "?" + Constants.AUTH_TOKEN + "=" + sessionManager.getSessionToken();
        url += "&" + Constants.EMAIL + "=" + sessionManager.getSessionEmail();
        Log.d(TAG, url);

        String postUrl = Constants.COMMAND + "=" + Constants.COMMAND_GET;
        postUrl += "&" + Constants.RETURN_VALUE_LIST + "=" + Constants.TRANSACTION_LIST;
        postUrl += "&" + Constants.PAGE_NAME + "=" + Constants.RECEIPTS;

        // Add options
        for (String key : options.keySet()) {
            postUrl += "&" + key + "=" + options.get(key);
        }
        Log.d(TAG, postUrl);
        try {
            JSONObject result = RestHelper.doPost(url, postUrl);
            buildTransactions(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Transaction> getTransactions() {
        return this.mTransactions;
    }


    protected void buildTransactions(JSONObject obj) throws JSONException {
        JSONArray transactionsList = obj.getJSONArray("transactionList");
        if (transactionsList == null)
            throw new JSONException("No transactionList");

        for (int i = 0; i < transactionsList.length(); i++) {
            Transaction transaction;
            try {
                transaction = new Transaction(transactionsList.getJSONObject(i));
                this.mTransactions.add(transaction);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }


}
